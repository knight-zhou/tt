package yeye.devops.model;

import lombok.Data;

@Data
public class User {
    private String name;
    private String city;
}
