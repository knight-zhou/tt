package yeye.devops.controller;

import com.alibaba.fastjson.JSONObject;
import netscape.javascript.JSObject;
import org.springframework.web.bind.annotation.*;
import yeye.devops.model.User;

import java.util.HashMap;
import java.util.Map;

/*
* 请求： curl -H "Content-Type:application/json"  -X POST 127.0.0.1:8080/api -d '{"city":"cs"}'
*
* */


@RestController
@RequestMapping("/api")
public class IndexController {
    @RequestMapping(value = "/js",method = RequestMethod.POST,produces = "application/json;charset=UTF-8")
    public  String js(@RequestBody User user){
        System.out.println(user.toString());
        System.out.println(user.getName()); // 获取用户名

        // 接受到的json数据封装一次再返回
        JSONObject res = new JSONObject();
        res.put("msg","ok");
        res.put("data",user);
        return res.toJSONString();
    }
}
