package yeye.devops;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class DevopsApplication {
    public static void main(String[] args) {

        SpringApplication.run(DevopsApplication.class, args);
        // 禁用启动日志
//        new SpringApplicationBuilder(DevopsApplication.class).logStartupInfo(false).run(args);
    }

}
